#!/usr/bin/python
# Rename ["g.wav", "g-001.wav", ...] based on dimensions.

import sys, os, errno, subprocess, operator, re, shutil


PITCH = 'pitch'
GROUP = 'group'
DYN = 'dyn'

forte = '96-127'
mezzo = '33-95'
piano = '1-32'

class reyong:
    dimensions1 = [
        (PITCH, ['c2']),
        (GROUP,
            ['open', 'mute+open', 'mute+closed', 'cek+closed', 'cek+open']),
        (DYN, [forte, mezzo, piano]),
        (GROUP, ['1', '2', '3']),
    ]
    dimensions2 = [
        (PITCH, ['d2']),
        (GROUP, ['open', 'mute+open', 'mute+closed']),
        (DYN, [forte, mezzo, piano]),
        (GROUP, ['1', '2', '3']),
    ]

class Pitch(object):
    def __init__(self, nn):
        self.nn = nn
    def __sub__(self, n):
        return Pitch(self.nn - n)
    def __add__(self, n):
        return Pitch(self.nn + n)
    def __repr__(self):
        oct, nn = divmod(self.nn, 12)
        ns = [
            'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b'
        ]
        # Native Instruments convention puts the lowest C and C-2, middle C
        # at C3.
        return '%s%d' % (ns[nn], oct - 2)
    def __str__(self):
        return str(int(self.nn))

C = Pitch(0)
oct = 12
D = C + 2
E = C + 4
F = C + 5
G = C + 7
A = C + 9
B = C + 11

# note 4 is closest to middle C
# F0 A0 B0
# C1 E1 F1 A1 B1
# C2 E2
#       F2 A2 B2
# C3 E3
#
# So C0 to B3, 3 octaves.
#
# C2 to B5 (f2, e4) - open
# C-2 to B1 (f-2, e0) - mute loose
# C6 to B9 (f6, e8) - mute
#   (G8 is max)
wayang_pitches = [
    F, A, B,
    C + oct, E + oct, F + oct, A + oct, B + oct,
    C + oct*2, E + oct*2
]
dyn4 = (DYN, ['1-31', '32-64', '65-108', '109-127'])
dyn6 = (DYN, ['1-21', '22-42', '43-64', '65-85', '86-106', '107-127'])

def wayang(articulation, variations, octave):
    return [
        (PITCH, [p + oct * (octave+2) for p in wayang_pitches]),
        (GROUP, [articulation]),
        dyn4,
        (GROUP, map(str, range(1, variations + 1))),
    ]

wayang_pemade_isep = [
    wayang('open', 4, 3),
    wayang('mute', 8, -1),
    wayang('calung', 3, 3),
    wayang('calung+mute', 6, -1),
    # wayang('loose', 8, -1),
], []

wayang_pemade_umbang = [
    wayang('open', 4, 3),
    wayang('calung', 3, 3),
    wayang('calung+mute', 6, -1),
    wayang('mute', 8, -1),
    wayang('loose', 8, -1),
], []

wayang_kantilan_isep = [
    wayang('open', 4, 4),
    wayang('mute', 8, 0),
    wayang('calung', 3, 4),
    wayang('calung+mute', 6, 0),
    wayang('loose', 8, 0),
], []

wayang_kantilan_umbang = [
    wayang('open', 4, 4),
    wayang('loose', 8, 0),
    wayang('calung', 3, 4),
    wayang('calung+mute', 6, 0),
    wayang('mute', 8, 0),
], ['%s-109-127-calung+mute%d' %(E + oct*4, i + 1) for i in range(6)]

mridangam = [
    # ki
    # ta
    # nam
    # din
    # arai (mising dyn1)
    # dheem dheem+stopped
    # thom+dry tha
    # thom
    # thom+low (missing one of dyn5)
    (GROUP, ['ki', 'ta', 'nam', 'din', 'arai', 'dheem', 'dheem+stopped',
        'thom+dry', 'tha', 'thom', 'thom+low']),
    (DYN, dyn6)
]
# ], [[(GROUP, 'arai'), (DYN, dyn6[0])], [

def main():
    print expand_dims([(GROUP, 'arai'), (DYN, dyn6)])
    return
    # print map(str, wayang_pitches)
    # rename('g.wav', 'g-000.wav')
    rename_dir('raw', 'renamed', wayang_kantilan_umbang)

def rename_dir(input, output, (dim_map, missing)):
    expected = sum(reduce(operator.mul, (len(xs) for (_, xs) in dims))
        for dims in dim_map) - len(missing)
    print 'expected:', expected
    print 'missing:', missing
    files = sorted((os.path.join(input, f)
            for f in os.listdir(input) if f.endswith('.wav')),
        key=sample_number)
    if len(files) != expected:
        raise Error('%d files, expected %d' % (len(files), expected))
    for dims in dim_map:
        files = rename_files(output, files, dims, missing)

def sample_number(fname):
    if fname.endswith('g.wav'):
        return 0
    return int(re.search(r'g-(\d+)\.wav$', fname).group(1))

def rename_files(out, files, dimensions, missing):
    missing = set(missing)
    dimensions = [
        map(lambda x: '%s-%s' % (tag, x), xs) for (tag, xs) in dimensions]
    names = [n for n in map(make_name, cartesian(dimensions))
            if n not in missing]
    subprocess.call(['mkdir', '-p', out])
    for old, new in zip(files, names):
        new = os.path.join(out, new) + '.wav'
        print old, '->', new
        # shutil.copy(old, new)
        os.link(old, new)
    return files[len(names):]

def expand_dims(dims):
    dims = [map(lambda x: '%s-%s' % (tag, x), xs) for (tag, xs) in dims]
    return [n for n in map(make_name, cartesian(dims))]

def make_name(attrs):
    def extract(prefix):
        return ''.join(
            attr.split('-', 1)[1] for attr in attrs
            if attr.startswith(prefix + '-'))
    pitch = extract(PITCH)
    dyn = extract(DYN)
    group = extract(GROUP)
    return '%s-%s-%s' % (pitch, dyn, group)

def cartesian(groups):
    if not groups:
        return []
    elif len(groups) == 1:
        return [[x] for x in groups[0]]
    else:
        xs, rest = groups[0], groups[1:]
        return [[x] + ps for x in xs for ps in cartesian(rest)]

# cartesian :: [[a]] -> [[a]]
# cartesian [] = []
# cartesian [xs] = [[x] | x <- xs]
# cartesian (xs:rest) = [x:ps | x <- xs, ps <- cartesian rest]

def rename(source, dest):
    try:
        os.rename(source, dest)
    except OSError, exc:
        if exc.errno != errno.ENOENT:
            raise


class Error(Exception): pass


if __name__ == '__main__':
    main()
