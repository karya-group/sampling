{-# LANGUAGE OverloadedStrings, GeneralizedNewtypeDeriving, LambdaCase #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{- | Given a directory of numbered samples already ordered by loudness by
    "Normalize", divide them into pitch and velocity ranges give them
    appropriate names so the kontakt auto-mapper can understand.

    Procedure:

    - @brew install normalize@

    - Move samples to @orig@, then cp to @norm@.

    - 'normalizeSubdirs' on @norm@ to normalize samples and create a db-map.
-}
module Normalize where
import Control.Monad
import Data.Function
import Data.Functor ((<$>))
import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe

import qualified System.FilePath as FilePath
import System.FilePath ((</>))
import qualified System.Process as Process

import Text.Printf (printf)

import qualified RE
import qualified Util



-- | Number of dynamic levels.
type Dynamics = Int
type Db = Double

-- * DbMap

type DbMap = Map.Map FilePath Db

saveDbMap :: FilePath -> DbMap -> IO ()
saveDbMap fn = writeFile fn . (++"\n") . show

loadDbMap :: FilePath -> IO DbMap
loadDbMap fn = read <$> readFile (fn </> "db-map")

normalizeSubdirs :: FilePath -> IO ()
normalizeSubdirs dir = do
    dirs <- Util.listDir dir
    mapM_ normalizeDir dirs

normalizeDir :: FilePath -> IO ()
normalizeDir dir = do
    files <- Util.listDir dir
    let saved = dir </> "db-map"
    when (saved `elem` files) $
        error $ "db-map already exists, directory is already normalized"
    saveDbMap saved =<< normalize files

-- | Normalize each sample to the loudest one and create a DbMap.
--
-- Run normalize cmd to get loudest, then normalize all to it.
normalize :: [FilePath] -> IO DbMap
normalize files = do
    stdout <- Process.readProcess "normalize" ("--no-adjust" : files) ""
    let dbmap = parseDbMap stdout
        loudest = maximum (Map.elems dbmap)
    putStrLn ""
    putStrLn $ "normalize to " ++ show loudest ++ ": " ++ head files
    putStrLn ""
    Process.readProcess "normalize"
        ("--quiet" : ("--amplitude=" ++ show loudest ++ "dBFS") : files) ""
    return dbmap

-- | Get DbMap from normalize cmd's stdout.
parseDbMap :: String -> DbMap
parseDbMap = Map.fromList . Maybe.mapMaybe parse . lines
    where
    parse line = case RE.match reg line of
        Just (_, [db, fn]) -> Just (fn, read db)
        _ -> Nothing
    reg = RE.compile "^(.*?)dBFS.* +(\\S+)$"

-- * renumber

renumberByDbSubdirs :: FilePath -> IO ()
renumberByDbSubdirs dir = mapM_ renumberByDb =<< Util.listDir dir

-- | Read the db-map and order the samples by their db, lowest to highest.
renumberByDb :: FilePath -> IO ()
renumberByDb dir = do
    db <- loadDbMap dir
    Util.rename
        [ (fn, makeName n)
        | (n, fn) <- zip [1..] $ map fst $ Util.sortOn snd (Map.toList db)
        ]

-- | Clean up files of the form @*[0-9].wav@.  Useful if I delete a sample
-- and the numbers now have gaps.
renumberDir :: FilePath -> IO ()
renumberDir dir = do
    files <- Util.listDir dir
    Util.rename (renumber files)

renumber :: [FilePath] -> [(FilePath, FilePath)]
renumber =
    concatMap number . List.groupBy ((==) `on` fst . fst) . List.sort
        . map split
    where
    number group = zip orig formatted
        where
        (new, orig) = unzip group
        formatted = map (\(n, fn) -> printf "%s%03d.wav" fn (n :: Int)) $
            zip [1..] $ map fst new
    split fn = (splitName fn, fn)

splitName :: FilePath -> (String, Int)
splitName fn = case RE.match reg fn of
    Just (_, [base, num]) -> (base, read num)
    _ -> error $ "doesn't match " ++ show reg ++ ": " ++ show fn
    where reg = RE.compile "(.*?)(\\d+)\\.wav$"

makeName :: Int -> FilePath
makeName = printf "s-%04d.wav"

-- * util

renumber_t0 = renumber (map (++".wav") ["a/a01", "a/b1", "a/a03", "a/a90"])

printDbMap :: FilePath -> IO ()
printDbMap dir = putStrLn . showDbMap =<< loadDbMap dir

showDbMap :: DbMap -> String
showDbMap =
    unlines . map elem . Util.sortOn snd . Map.toAscList
    where elem (fn, db) = FilePath.takeFileName fn ++ "\t" ++ show db
