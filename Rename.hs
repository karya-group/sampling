module Rename where
import Control.Monad hiding (join)
import qualified Data.List as List
import qualified Data.Map as Map
import qualified System.FilePath as FilePath

import qualified Util


-- | Allot the groups over the total range.
makePitches :: Pitch -- ^ start mapping at this pitch
    -> Pitch -- ^ natural sample pitch is at this offset in the range
    -> Pitch -- ^ size of each group
    -> [[Group]] -> GroupMap
makePitches start root range groupsByPitch = Map.fromList $ do
    (n, groups) <- zip [0..] groupsByPitch
    let base = start + n*range
    (ks, group) <- zip [0..] groups
    let end = base + range - 1
    when (end > 127) $
        error $ "group out of range: " ++ show (group, base, end)
    return (group, (ks, (base, base + root, end)))


-- * implementation

-- | Map a group name to its keyswitch, bottom of range, sample root, and top
-- of range.
type GroupMap = Map.Map Group (Keyswitch, (Pitch, Pitch, Pitch))

type Pitch = Int
type Velocity = Int
type Group = String
-- | Keyswitch index, starting at 0.
type Keyswitch = Int

-- | Look for subdirs with group names.  Assume each dir contains samples,
-- numbered in order of increasing velocity.  Rename them with pitch and
-- velocity ranges in the filename so kontakt can map them automatically.
--
-- Use 'getSubdirRenames' first to verify.
renameSubdirs :: GroupMap -> FilePath -> IO ()
renameSubdirs groups parent = Util.rename =<< getSubdirRenames groups parent

getSubdirRenames :: GroupMap -> FilePath -> IO [(FilePath, FilePath)]
getSubdirRenames groups parent = do
    dirs <- Util.listDir parent
    concat <$> mapM (getDirRenames groups) dirs

-- | Create a rename list for the given dir, which must be in the GroupMap.
getDirRenames :: GroupMap -> FilePath -> IO [(FilePath, FilePath)]
getDirRenames groups dir = do
    val <- maybe (Util.throw $ "no group for " ++ dir) return $
        Map.lookup (FilePath.takeFileName dir) groups
    fns <- Util.listSampleDir dir
    let variations = 1
    names <- maybe (Util.throw "fns not divisible by variations") return $
        groupNames (FilePath.takeFileName dir) val variations (length fns)
    return $ zip fns (map (++".wav") names)

-- | Create filenames for the given group.  They look like
-- @low_pitch-root_pitch-high_pitch-low_vel-high_vel-name@.
-- If there is >1 variation, the name will be suffixed with the variation
-- number.
groupNames :: Group -> (Keyswitch, (Pitch, Pitch, Pitch)) -> Int -> Int
    -> Maybe [FilePath]
groupNames name (_ks, (lowp, rootp, highp)) variations nfiles
    | nfiles `mod` variations /= 0 = Nothing
    | otherwise = Just $ do
        (lowv, highv) <- velRanges (nfiles `div` variations)
        var <- [1..variations]
        return $ join "-" $
            map show [lowp, rootp, highp, lowv, highv]
                ++ [name ++ if variations <= 1 then "" else "+v" ++ show var]

-- | Split the total velocity range into the given number of regions.
velRanges :: Int -> [(Velocity, Velocity)]
velRanges dynamics = zip starts (map (subtract 1) (drop 1 starts))
    where
    starts = scanl (+) 0 groups
    groups = integralGroups 128 dynamics

-- * util

integralGroups :: Int -- ^ distribute this many units
    -> Int -- ^ among this many buckets
    -> [Int]
integralGroups num denom =
    replicate extra (per+1) ++ replicate (denom - extra) per
    where
    (per, extra) = num `divMod` denom

join :: [a] -> [[a]] -> [a]
join = List.intercalate

join2 :: [a] -> [a] -> [a] -> [a]
join2 sep xs ys
    | null xs = ys
    | null ys = xs
    | otherwise = xs ++ sep ++ ys
