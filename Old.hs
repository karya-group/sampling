module Old where


-- The old way used a hardcoded set of dynamics with velocity ranges.  I then
-- distributed the samples evenly among them.  The new way, each Group has
-- a different number of samples and I automatically create velocity ranges.


-- * mridangam

mridangamDims :: DimMap
mridangamDims = DimMap
    { dmDims = dims
    , dmMissing =
        [[(Directory, ["thom+low"]),
            (Velocity, [vels !! 4]), (Group, ["low+v4"])]]
    }
    where
    dims = concat
        [ tagged "" ["ki", "ta", "nam", "din", "arai", "dheem"]
        , tagged "staccato" ["dheem+staccato"]
        , tagged "dry" ["thom+dry"]
        , tagged "left" ["tha", "thom"] -- left hand in a separate group
        , tagged "low" ["thom+low"]
        ]
    vels = [fmt lowv ++ "-" ++ fmt highv | (lowv, highv) <- vels_]
    vels_ = [(0, 20), (21, 41), (42, 63), (64, 84), (85, 114), (115, 127)]
    tagged tag dirs = do
        dir <- dirs
        let (lowp, rootp, highp) = Maybe.fromMaybe
                (error $ "lookup: " ++ dir) $ Map.lookup dir mridangamPitches
        let v = if dir == "arai" then tail vels else vels
        return
            [ (Directory, [dir])
            , (Pitch, [join "-" (map fmt [lowp, rootp, highp])])
            , (Velocity, v)
            , group tag
            ]
    fmt :: Int -> String
    -- fmt n = printf "%03d" n
    fmt = show
    group tag = (Group, [join2 "+" tag ('v' : show n) | n <- [1..4]])


type Dimension = (Tag, [String])
data Tag = Pitch | Group | Velocity | Directory deriving (Eq, Show)

data DimMap = DimMap {
    dmDims :: [[Dimension]]
    , dmMissing :: [[Dimension]]
    } deriving (Show)

-- output: pitch-dyn-group, e.g. c3-0-20-ki1

mridangamDir = "/Users/elaforge/Music/mix/mridangam2/raw"
mridangamOut = "/Users/elaforge/Music/mix/mridangam2/out"
renameMridangam = renameDir mridangamDims mridangamDir mridangamOut

t0 = concatMap expandDims (dmDims mridangamDims)
t1 = expandDimMap mridangamDims

-- | Rename samples 
renameDir :: DimMap -> FilePath -> FilePath -> IO ()
renameDir dimMap inDir outDir = do
    let outFns = [outDir </> fn ++ ".wav" | fn <- expandDimMap dimMap]
    fns <- listSampleDir inDir
    when (length fns /= length outFns) $
        throw $ "expected " ++ show (length outFns) ++ " but dir has "
            ++ show (length fns)
    let outDirs = Set.toList $ Set.fromList $ map FilePath.dropFileName outFns
    mapM_ (Directory.createDirectoryIfMissing True) outDirs
    mapM_ (uncurry rename) (zip fns outFns)
    where
    rename old new = do
        putStrLn $ "cp " ++ old ++ " -> " ++ new
        Directory.copyFile old new

expandDimMap :: DimMap -> [FilePath]
expandDimMap (DimMap dims missing) =
    filter (not . isMissing) (concatMap expandDims dims)
    where
    isMissing d = any (`List.isPrefixOf` d) missingPrefixes
    missingPrefixes = concatMap expandDims missing

expandDims :: [Dimension] -> [FilePath]
expandDims dims
    | null dirs = expanded
    | otherwise = expandWith "/" [dirs, expanded]
    where
    expanded = expandWith "-" $ filter (not . null) [pitch, dyn, group]
    expandWith sep = map (join sep) . cartesian
    dirs = extract Directory
    pitch = extract Pitch
    dyn = extract Velocity
    group = extract Group
    extract tag = concat [vals | (t, vals) <- dims, t == tag]


-- groupNames :: [((Pitch, Pitch, Pitch), Group)] -> .
-- groupNames = concatMap expandGroup
--     where
--     expandGroup (pitches, group) = do
--         ((lowv, highv), files) <-

-- groupNames :: FilePath -> (Pitch, Pitch, Pitch)
--     -> [((Velocity, Velocity), [FilePath])] -> Group -> IO ()
-- groupNames dir (lowp, rootp, highp) vels group =
--     ((lowv, highv), files) <- vels

-- Normalize each directory, and create renamed links in the out dir.
-- process :: FilePath -> IO ()
-- process dir = mapM_ processGroup mridangamPitches
--     where
--     processGroup (pitches, group) = do
--         dbmap <- normalizeDir (dir </> group)
--         let dyns = groupDyn dynamics dbmap
--         setName (FilePath.takeDirectory dir </> "out") pitches
--             (zip vels dyns) group
--     vels = velRanges dynamics
--     dynamics = 7

velRanges :: Int -> [String]
velRanges splits = [printf "%d-%d" s e | (s, e) <- pairs]
    where
    d = 127 / fromIntegral (splits :: Int)
    starts = [round (d * fromIntegral n) | n <- [0..splits-1]]
    pairs :: [(Int, Int)]
    pairs = zip starts (map (subtract 1) (drop 1 starts) ++ [127])


setName :: FilePath -> (Pitch, Pitch, Pitch)
    -> [((Velocity, Velocity), [FilePath])] -> Group -> IO ()
setName dir (lowp, rootp, highp) vels group = do
    Directory.createDirectoryIfMissing True (dir </> group)
    forM_ vels $ \((lowv, highv), files) ->
        forM_ (zip [1..] files) $ \(n, file) -> do
            let name = join "-"
                    [ show lowp, show rootp, show highp
                    , show lowv, show highv, group ++ printf "%02d" (n :: Int)
                    ]
                out = dir </> group </> name ++ ".wav"
            putStrLn $ file ++ " -> " ++ out
            Directory.copyFile file out

groupDyn :: Ord v => Int -> Map.Map k v -> [[k]]
groupDyn dynamics = integralChunks dynamics
    . map fst . List.sortBy (compare `on` snd) . Map.toList

integralChunks :: Int -> [a] -> [[a]]
integralChunks size xs =
    snd $ List.mapAccumL go xs $ integralGroups (length xs) size
    where go xs group = Tuple.swap $ splitAt group xs

mridangamGroups :: [[Group]]
mridangamGroups =
    [ ["tha"], ["thom", "thom+low", "thom+dry"]
    , ["ta"], ["ki"], ["nam"], ["din"]
    , ["arai", "muru"], ["dheem", "dheem+staccato"], ["meetu"]
    ]

mridangamPitches :: Map.Map Group (Pitch, Pitch, Pitch)
mridangamPitches = Map.fromList $ do
    (n, groups) <- zip [0..] mridangamGroups
    let base = n * 12
    group <- groups
    return (group, (low + base, root + base, high + base))
    where
    low = 12
    root = low + 8
    high = low + 11


-- * util

-- | cartesian ["ab", "12", "xy"]
-- ["a1x","a1y","a2x","a2y","b1x","b1y","b2x","b2y"]
cartesian :: [[a]] -> [[a]]
cartesian [] = []
cartesian [xs] = [[x] | x <- xs]
cartesian (xs:rest) = [x:ps | x <- xs, ps <- cartesian rest]
