module Normalize where
import qualified Control.Exception as Exception
import qualified Control.Monad.Trans as Trans
import qualified Data.Int as Int
import qualified Sound.PortAudio as PortAudio


main :: IO ()
main = (right =<<) $ PortAudio.withPortAudio $
    PortAudio.withDefaultStream 0 2 sr Nothing callback Nothing normalize
    where
    sr = 44100
    callback = Nothing

{-
    Load a reference sample
    Load testee, alternate 

    play thread accepts samples on chan and plays them

    display shows current sample and reference, key alternates between them,
    up down increments/decrements volume

    Then move on, and write the amount of amplification to a file.

    Process program takes a desired velocity range and a amp->vel mapping.
    It reads the samples, gives them ranges, and turns them into vel ranges.
    Then it clusters vel points to tweak the breakpoints into the same place.

    So wider vel range means more overlaps, which means more variations.
    Narrower vel range means fewer, but more layers.

    Then emit MIDI at all vel transitions to listen first to all variations,
    then to transitions.
-}

normalize :: PortAudio.Stream Int.Int16 Int.Int16
    -> IO (Either PortAudio.Error ())
normalize stm = do
    just =<< PortAudio.startStream stm
    -- just =<< PortAudio.writeStream frames fptr
    just =<< PortAudio.stopStream stm
    return $ Right ()

right :: Either PortAudio.Error a -> IO a
right = either (errorIO . show) return

just :: Maybe PortAudio.Error -> IO ()
just = maybe (return ()) (errorIO . show)

errorIO :: (Trans.MonadIO m) => String -> m a
errorIO = Trans.liftIO . Exception.throwIO . Exception.ErrorCall
