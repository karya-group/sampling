module Util where
import qualified Control.Exception as Exception
import Control.Monad
import qualified Data.Either as Either
import qualified Data.List as List
import qualified Data.Map as Map

import qualified System.Directory as Directory
import qualified System.FilePath as FilePath
import System.FilePath ((</>))

import qualified RE


-- * dir

mapSubdirs :: (FilePath -> IO a) -> FilePath -> IO ()
mapSubdirs f parent = mapM_ f =<< listDir parent

-- | Rename files in a directory.  The are renamed into an intermediate
-- directory, so a->b and b->a will swap them as expected.
--
-- The input should be like [("dir/from", "to"), ...]
rename :: [(FilePath, FilePath)] -> IO ()
rename renames = forM_ byDir $ \(dir, renames) -> do
    let tmp = dir </> "tmp"
    Directory.createDirectoryIfMissing True tmp
    forM_ renames $ \(old, new) ->
        Directory.renameFile (dir </> old) (tmp </> new)
    files <- listDir tmp
    forM_ files $ \file -> Directory.renameFile file (parentDir file)
    Directory.removeDirectory tmp
    where
    byDir = keyedGroup (FilePath.takeDirectory . fst) renames

relink :: FilePath -> FilePath -> FilePath -> [(FilePath, FilePath)] -> IO ()
relink baseDir fromDir toDir renames = forM_ renames $ \(old, new) ->
    Directory.createFileLink (baseDir </> fromDir </> old)
        (baseDir </> toDir </> new)

listDir :: FilePath -> IO [FilePath]
listDir dir = map (dir</>) . filter ((/=".") . take 1) <$>
    Directory.getDirectoryContents dir

-- | a/b/c -> a/c
parentDir :: FilePath -> FilePath
parentDir fn = FilePath.takeDirectory (FilePath.takeDirectory fn)
    </> FilePath.takeFileName fn

listSampleDir :: FilePath -> IO [FilePath]
listSampleDir dir =
    List.sortBy (\a b -> compare (sampleNumber a) (sampleNumber b))
        . filter (".wav" `List.isSuffixOf`) <$> listDir dir

listSampleDirNumbers :: FilePath -> IO [(FilePath, Int)]
listSampleDirNumbers dir = do
    fns <- filter (".wav" `List.isSuffixOf`) <$> listDir dir
    let (errs, ok) = Either.partitionEithers $
            map (\fn -> maybe (Left fn) (Right . (,) fn) (sampleNumber fn)) fns
    if null errs then return ok else error $ "no sample numbers: " ++ show errs

sampleNumber :: FilePath -> Maybe Int
sampleNumber fn = case RE.match reg fn of
    Just (_, [n]) -> Just $ read n
    _   | ".wav" `List.isSuffixOf` fn -> Just 0
        | otherwise -> Nothing
    where reg = RE.compile "-(\\d+)\\.wav$"

-- * list

sortOn :: Ord k => (a -> k) -> [a] -> [a]
sortOn k = List.sortBy (\a b -> compare (k a) (k b))

groupOn :: Eq k => (a -> k) -> [a] -> [[a]]
groupOn k = List.groupBy (\a b -> k a == k b)

keyedGroup :: Ord key => (a -> key) -> [a] -> [(key, [a])]
keyedGroup key = Map.toAscList . foldr go Map.empty
    where go x = Map.alter (Just . maybe [x] (x:)) (key x)

-- * Misc

throw :: String -> IO a
throw = Exception.throwIO . Exception.ErrorCall

second f (a, b) = (a, f b)
first f (a, b) = (f a, b)
