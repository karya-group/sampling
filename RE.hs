module RE where
import qualified Data.ByteString.Char8 as B
import qualified Text.Regex.PCRE.Light as PCRE


compile :: String -> PCRE.Regex
compile re = PCRE.compile (B.pack re) []

match :: PCRE.Regex -> String -> Maybe (String, [String])
match re text = do
    all : matches <- map B.unpack <$> PCRE.match re (B.pack text) []
    return (all, matches)
