module Renumber where
import qualified System.FilePath as FilePath
import qualified Text.Printf as Printf

import qualified Util


dir = "/Users/elaforge/Music/mix/reyong/raw-high"
t0 = add 1224 dir

add :: Int -> FilePath -> IO ()
add increment dir = do
    fns <- Util.listSampleDirNumbers dir
    Util.rename [(fn, makeFn fn (increment + n)) | (fn, n) <- fns]

makeFn :: FilePath -> Int -> FilePath
makeFn fn n = FilePath.takeDirectory fn ++ Util.makeName n
