-- | Renaming for pitched sample sets.  This takes a tree of variants along
-- various dimensions, and expands all of their permutations.
module Pitched where
import Control.Monad
import qualified Data.Set as Set
import qualified System.FilePath as FilePath
import qualified Text.Printf as Printf

import qualified Key
import qualified Util


-- * types

data Dimension =
    Pitch [Key.Key] | Dyn [(Int, Int)] | Var Int | Art [String]
    deriving (Show)
data D = D Dimension [D]
    deriving (Show)

-- | Key (lowVel, highVel) group variation
data Group = Group Key.Key (Int, Int) String Int deriving (Eq, Ord, Show)

-- | Kontakt splits on _ and -.  It is extemely confused by leading 0s.
groupFilename :: Group -> FilePath
groupFilename (Group (Key.Key key) (low_vel, high_vel) art var) =
    Printf.printf "%d-%d-%d-%s+v%d.wav" key low_vel high_vel art var

enumerate :: D -> IO ()
enumerate = mapM_ print . map head . group . zip [1..] . permuteD
    where
    print (i, Group key _vel g _var) =
        putStrLn $ show i ++ ": " ++ show key ++ " " ++ g
    group [] = []
    group gs = pre : group post
        where (pre, post) = spanOn (key . snd) gs
    key (Group k _vel g _var) = (k, g)

spanOn :: Eq k => (a -> k) -> [a] -> ([a], [a])
spanOn key xs@(x:_) =
    (takeWhile ((== key x) . key) xs, dropWhile ((== key x) . key) xs)
spanOn _ [] = ([], [])

printGroupIndices :: [Group] -> IO ()
printGroupIndices = putStr . unlines . concatMap fmtGroup
    . Util.groupOn (byKey . snd) . groupIndices
    where
    fmtGroup (g:gs) = fmt g : map (("  "++) . fmt) gs
    fmtGroup [] = []
    fmt (i, group) = show i ++ ": " ++ show group
    byKey (Group key _ _ _) = Group key (0, 0) "" 0
    -- byKeyVel (Group key vel _ _) = Group key vel "" 0

groupIndices :: [Group] -> [(Int, Group)]
groupIndices groups = zip nums (map head gs)
    where
    gs = Util.groupOn (\(Group key vel group _) -> Group key vel group 0)
        groups
    counts = map length gs
    nums = scanl (+) 1 counts

sgroupIndices :: Int -> [[String]] -> [(Int, [String])]
sgroupIndices depth =
    map extract . Util.groupOn (take depth . snd) . zip [1..]
    where
    extract ((i, group) : _) = (i, group)
    extract [] = error "empty group"

-- * rename

renames :: [Group] -> FilePath -> IO [(FilePath, FilePath)]
renames groups dir = do
    samples <- Util.listSampleDir dir
    unless (length groups == length samples) $
        Util.throw $ "expected " ++ show (length groups)
            ++ " samples, but found " ++ show (length samples)
    return $ zip samples (map groupFilename groups)

renamesF :: FilePath -> [FilePath] -> IO [(FilePath, FilePath)]
renamesF dir groups = do
    samples <- Util.listSampleDir dir
    unless (length groups == length samples) $
        putStrLn $ "expected " ++ show (length groups)
            ++ " samples, but found " ++ show (length samples)
    return $ zip (map FilePath.takeFileName samples) groups

-- | Get all permutations, and subtract missing ones.
permuteMissing :: (D, D) -> [Group]
permuteMissing (d, missingD) =
    filter (not . (`Set.member` missing)) $ permuteD d
    where
    missing = Set.fromList (permuteD missingD)

permuteDs :: [D] -> [Group]
permuteDs = concatMap permuteD

permuteD :: D -> [Group]
permuteD = map collect . cartesianD

cartesianD :: D -> [[Dimension]]
cartesianD (D dim []) = [[d] | d <- expand dim]
cartesianD (D dim subs) = prepend (expand dim) (concatMap cartesianD subs)

prepend :: [a] -> [[a]] -> [[a]]
prepend xs ys = [x:y | x <- xs, y <- ys]

collect :: [Dimension] -> Group
collect ds = Group key dyn art var
    where
    [key] = [key | Pitch [key] <- ds]
    [dyn] = [dyn | Dyn [dyn] <- ds]
    [var] = [var | Var var <- ds]
    [art] = [art | Art [art] <- ds]

expand :: Dimension -> [Dimension]
expand d = case d of
    Pitch keys -> map (Pitch . (:[])) keys
    Dyn ranges -> map (Dyn . (:[])) ranges
    Var n -> map Var [1..n]
    Art arts -> map (Art . (:[])) arts

-- | The cartesian product of a list of lists.  E.g.
-- @[[1, 2], [3, 4]]@ -> @[[1, 3], [1, 4], [2, 3], [2, 4]]@.
cartesian :: [[a]] -> [[a]]
cartesian [] = []
cartesian [xs] = [[x] | x <- xs]
cartesian (xs:rest) = [x:ps | x <- xs, ps <- cartesian rest]


-- * string dimensions

data SD = SD [String] [SD]

permuteSDs :: [SD] -> [[String]]
permuteSDs = concatMap permuteSD

permuteSD :: SD -> [[String]]
permuteSD (SD ds []) = [[d] | d <- ds]
permuteSD (SD ds subs) = prepend ds (concatMap permuteSD subs)
