{-# LANGUAGE TupleSections #-}
module Inst where
import Data.Bifunctor (first)
import qualified Data.Char as Char
import qualified Data.List as List
import Data.List ((\\))

import System.FilePath ((</>))

import qualified Key
import qualified Normalize
import qualified Pitched
import Pitched (Dimension(..), D(..), SD(..))
import qualified Rename
import qualified Util


rename :: Rename.GroupMap -> FilePath -> Bool-> IO ()
rename groups dir doActions
    | doActions = Rename.renameSubdirs groups dir
    | otherwise = mapM_ print =<< Rename.getSubdirRenames groups dir

renameP :: [Pitched.Group] -> FilePath -> Bool -> IO ()
renameP groups dir doActions
    | doActions = Util.rename =<< Pitched.renames groups dir
    | otherwise = mapM_ print =<< Pitched.renames groups dir

relink :: FilePath -> FilePath -> FilePath -> [FilePath] -> IO ()
relink baseDir fromDir toDir samples =
    Util.relink baseDir fromDir toDir
        =<< Pitched.renamesF (baseDir </> fromDir) samples

normalize :: FilePath -> IO ()
normalize = Normalize.normalizeSubdirs

baseDir :: FilePath
baseDir = "/Users/elaforge/Music/mix/sample"

join :: [String] -> FilePath
join = (<>".wav") . List.intercalate "-"

{-
    3i 3o 3e 3u 3a 4i 4o 4e 4u 4a 5i 5o 5e 5u 5a 6i 6o 6e 6u 6a 7i
    jegog---------
                   calung--------
                                  penyacah------
       ugal-------------------------
          rambat-----------------------------------
    0              7              14             21             28
    3i 3o 3e 3u 3a 4i 4o 4e 4u 4a 5i 5o 5e 5u 5a 6i 6o 6e 6u 6a 7i
                trompong---------------------
                      pemade-----------------------
                                     kantilan---------------------
                         reyong-----------------------------
                         |1-----|---       |3--|---
                                  |2-----|---    |4--------|
    3i 3o 3e 3u 3a 4i 4o 4e 4u 4a 5i 5o 5e 5u 5a 6i 6o 6e 6u 6a 7i
-}

-- * ceng ceng

rincikDir = baseDir </> "rincik"
-- open, rl: 24 total, 12 R, 12 L
-- seems: 7 + 10 + 7 ?  Or just a continuum.
-- 7 + 9 + 8
--
-- how about: 6 + 8 + 14
--
-- closed, rl: 14 soft, 8 medium, 8 loud
-- both open: 6 p, 6 m, 6 f
-- both closed: 6 p, 6 m, 6 f
-- muted, rl: 8 p m f, and again 8 p m f
-- kopyak closed, 8 p m f
-- kopyak open, 8 p m f

rincikGroups = concat
    [ [ join ["open+" <> hand, dyn, var]
      | dyn <- dyns
      , (hand, var) <- handVars 8
      ]
    , [ join ["closed+" <> hand, dyn, var]
      | (dyn, hvs) <- zip dyns (map handVars [14, 8, 8])
      , (hand, var) <- hvs
      ]
    , [ join [art, dyn, var]
      | art <- ["open+both", "closed+both"]
      , dyn <- dyns
      , var <- vars 6
      ]
    , [ join ["mute+" <> hand, dyn, var]
      | dyn <- dyns, (hand, var) <- handVars 8
      ]
    , [ join ["mute+" <> hand, dyn, var]
      | dyn <- dyns, (hand, var) <- drop 8 (handVars 16)
      ]
    , [ join ["kopyak+" <> art, dyn, var]
      | art <- ["closed", "open"], dyn <- dyns, var <- vars 8
      ]
    ]
    where
    handVars n =
        [(hand, var) | var <- vars (n `div` 2), hand <- ["right", "left"]]
    dyns = ["p", "m", "f"]
rincikRename = relink (baseDir </> "rincik") "numbered" "out" rincikGroups

kopyakGroups_old =
    [ join [art, dyn, var]
    | art <- ["open", "closed"]
    , dyn <- ["pp", "mp", "mf", "ff"]
    , var <- vars 4
    ]
kopyakGroups =
    [ show inst <> "/" <> join [art, dyn, var]
    | inst <- [1, 2, 3]
    , art <- ["closed", "open", "rim"]
    , dyn <- dyns
    , var <- vars $ case (inst, art) of
        (_, "closed") -> 8
        (1, "open") -> 6
        (1, "rim") -> dynVars dyn [7, 6, 7, 7]
        (2, "open") -> dynVars dyn [8, 7, 8, 7]
        (2, "rim") -> dynVars dyn [8, 8, 6, 8]
        (3, "open") -> dynVars dyn [8, 7, 8, 8]
        (3, "rim") -> dynVars dyn [8, 7, 7, 7]
    ]
    where
    dyns = ["pp", "mp", "mf", "ff"]
    dynVars dyn vals = (vals!!) $ case List.elemIndex dyn dyns of
        Just i -> i
kopyakRename = relink (baseDir </> "kopyak") "numbered" "out" kopyakGroups

{-
kopyak1:
    * closed, 4 dyn, 8 var
    * open, 4 dyn, 6 var
    * rim, 4 dyn, 7 var
        pp 7, mp 6, mf 7, ff 7

kopyak2:
    closed, 4 dyn, 8 var
    open, 4 dyn, 8 var
        pp 8, mp 7, mf 8, ff 7
    rim, 4 dyn, 8 var
        pp 8, mp 7, mf 6, ff 8

kopyak3:
    * closed, 4 dyn, 8 var
    * open, 4 dyn,
        pp 8, mp 7, mf 8, ff 8
    * rim, 4 dyn,
        pp 8, mp 7, mf 7, ff 7
-}

-- * gender rambat

{-
    . Normalized via: normalize --amplitude=-24dBFS *.wav

    . tunings: umbang, isep
    . notes: 14: 0e u a 1i o e u a 2i o e u a 3i
    . articulations: gender+open, gender+mute,
    gangsa+open?, gangsa+mute+open, gangsa+mute+closed
    calung+open, calung+mute
    . dynamics: pp, mp, mf, ff
    . variations: 4

    So each articulation adds 448 samples.
    Muted ones are much cheaper than open ones though.

    mic1 high: 44, from rambat: 7", level = 29
    mic2 high: 25, from rambat: 64", level = 37

    7 notes from bottom, without calung panggul

    I can record 5 short (3s) notes, and then 2 full decay ones.  Then
    put the long ones in their own articulation, which is triggered by
    a long-enough note.

    Record 5 shorts, 1 long.

    +open long
    +open short * 5
    +mute+gender+open * 5
    +mute+gender+closed * 5

    actually:
    +open, 4 dyn, 4 var
    +mute+gender+loose, 4 dyn, 4 var
    +mute+gender, 4 dyn, 4 var
    +mute+gangsa, 4 dyn, 4 var

    then other instrument
    +open, 4 dyn, 4 var short, 1 var long
    shorts I think are all >2s

    calung, umbang?
    +calung+open 4 dyn, 4 var (all long?)
    +calung+mute 4 dyn, 4 var

    calung, isep?
-}

renameRambat :: IO ()
renameRambat = sequence_
    [ relink (rambatDir </> dir) "numbered" "out" (map join groups)
    | (dir, groups) <- rambatGroups
    ]

rambatDir = baseDir </> "rambat"

rambatIndices = mapM_ print $ take 32 $ dropWhile ((<correctUntil) . fst) $
    map (first (+ 4)) $
    Pitched.sgroupIndices depth (snd (rambatGroups !! 2))
    where
    depth = 2
    correctUntil = 256

type Group = [String]

rambatUmbang = snd $ rambatGroups !! 0
rambatIsep = snd $ rambatGroups !! 1

rambatGroups :: [(FilePath, [Group])]
rambatGroups =
    [ ("umbang",) $ concat
        [ [ [pitch, art, dyn, var]
          | pitch <- symPitches (P 3 E) (P 4 E)
          , art <- "open" : mutes
          , dyn <- dyns
          , var <- vars 4
          ]
        , withShort (symPitches (P 4 U) (P 6 I))
        ]
    , ("isep", withShort (symPitches (P 3 E) (P 6 I)))
    , ("umbang-calung", Pitched.permuteSDs
        [ SD (symPitches (P 3 E) (P 6 I))
            [ SD [calung] [SD dyns [SD (vars 3) []]]
            , SD [mute] [SD dyns [SD (vars 4) []]]
            ]
        ]
      )
    , ("isep-calung", Pitched.permuteSDs
        [ SD (symPitches (P 3 E) (P 3 U))
            [ SD [calung, mute] [SD dyns [SD (vars 4) []]]
            ]
        , SD (symPitches (P 3 A) (P 6 I))
            [ SD [calung] [SD dyns [SD (vars 3) []]]
            , SD [mute] [SD dyns [SD (vars 4) []]]
            ]
        ]
        \\
        [ ["4i", mute, "ff", "v4"]
        ]
      )
    ]
    where
    withShort pitches =
      [ [pitch, art, dyn, var]
      | pitch <- pitches
      , (art, dyn, var) <- concat
        [ [ (art, dyn, var)
          | dyn <- dyns
          , (art, var) <- concat
            [ [("open+short", var) | var <- vars 4]
            , [("open", var) | var <- vars 1]
            ]
          ]
        , [ (art, dyn, var) | art <- mutes, dyn <- dyns, var <- vars 4]
        ]
      ]
    mutes = ["mute+gender+loose", "mute+gender+tight", "mute+gangsa"]
    dyns = ["pp", "mp", "mf", "ff"]
    calung = "calung"
    mute = "calung+mute"

vars n = map (('v':) . show) [1..n]

symPitches :: P -> P -> [String]
symPitches low high = map showP [low .. high]
    where showP (P oct p) = show oct <> map Char.toLower (show p)

data P = P Int Selisir
    deriving (Eq, Ord, Show)

instance Enum P where
    fromEnum (P oct p) = oct * 5 + fromEnum p
    toEnum n = P oct (toEnum p)
        where (oct, p) = n `divMod` 5

data Selisir = I | O | E | U | A
    deriving (Eq, Ord, Show, Enum, Bounded)


-- * reyong

-- Fireface 400, mic1 overhead level=19, mic2 underneath level=10
{-
    96 samples per note
    * 15 notes = 1440
-}

reyongGroupsOld = Pitched.permuteMissing reyongDimensionsOld

reyongDir = baseDir </> "reyong/renamed"

renameReyong = renameP reyongGroups reyongDir
normalizeReyong = Normalize.normalizeDir reyongDir
reyongGroups = Pitched.permuteD reyongDimensions

reyongDimensions :: D
reyongDimensions = fst reyongDimensionsOld

-- articulations: open, mute+closed, mute+open, cek+closed, cek+open
-- dyn: p, mp, mf, f
-- var: record 5, use 4
-- cek: record 8, use 6

-- open - 1         16
-- mute+closed - 17
-- mute+open - 33
-- cek+closed - 57
-- cek+open - 81

reyongDimensionsOld :: (D, D)
reyongDimensionsOld =
    ( D (Pitch pitches)
        [ D (Art [open, mute_closed, mute_open]) [D dyns [D (Var 4) []]]
        , D (Art [cek_closed, cek_open]) [D dyns [D (Var 6) []]]
        ]
    -- subtract missing ones
    , D (Pitch [oct 4 a]) [D (Art [cek_open]) [D dyns [D (Var 6) []]]]
    )
    where
    pitches = oct 3 a : map (oct 4) [i, o, e, u, a]
        ++ map (oct 5) [i, o, e, u, a]
        ++ map (oct 6) [i, o, e, u]
    (i, o, e, u, a) = (Key.c_1, Key.d_1, Key.e_1, Key.g_1, Key.a_1)
    oct n k = k + (n+1) * 12
    dyns = Dyn [(1, 31), (32, 64), (65, 108), (109, 127)]
    -- This is the actual divisions, but I keep the old ones since kontakt
    -- depends on the filenames.
    -- dyn = Dyn [(1, 31), (32, 64), (65, 112), (113, 127)]

    -- 4 var
    open = "open"
    mute_closed = "mute+closed"
    mute_open = "mute+open"
    -- 6 var
    cek_closed = "cek+closed"
    cek_open = "cek+open"

-- * kajar

kajarDir = baseDir </> "kajar/norm"
renameKajar = rename kajarGroups kajarDir

kajarGroups :: Rename.GroupMap
kajarGroups = Rename.makePitches 24 6 12
    [ ["center+closed"]
    , ["center+open"]
    , ["rim+closed"]
    , ["rim+open"]
    , ["rim+staccato"]
    , ["damp"]
    ]

-- * mridangam

mridangamDir = baseDir </> "mridangam-d/norm"
normalizeMridangam = Normalize.normalizeSubdirs mridangamDir
renameMridangam = rename mridangamGroups mridangamDir

mridangamGroups :: Rename.GroupMap
mridangamGroups = Rename.makePitches 12 6 12
    [ ["tha"]
    , ["thom", "gumki", "gumki+up"]
    , ["ki"]
    , ["ta"]
    , ["nam"]
    , ["din"]
    , ["chapu"]
    , ["meetu+ki", "dheem"]
    , ["meetu+ta"]
    ]

-- * kendang sunda

sundaDir = baseDir </> "kendang-sunda/norm"
normalizeSunda = normalize sundaDir
renameSunda = rename sundaGroups sundaDir

sundaGroups :: Rename.GroupMap
sundaGroups = Rename.makePitches 8 6 12
    -- indung, left
    [ ["dong", "det1", "det2", "det3", "det4", "det5"] -- tuned via controller
    , ["ting"]
    , ["tak"] -- closed

    -- indung, right
    , ["phak"]
    , ["phak1"] -- phak with one finger
    , ["ping"]
    , ["pong"]
    -- kulanter gede
    , ["tung"]
    -- kulanter leutik
    , ["pak"]
    , ["peung"]
    ]

-- * kendang bali

renameKendang = rename kendangGroups kendangDir
kendangDir = baseDir </> "kendang-legong/lanang"

kendangGroups :: Rename.GroupMap
kendangGroups = Rename.makePitches 12 6 12
    [ ["de+staccato", "plak"] -- right
    , ["de+thumb", "dag+staccato"]
    , ["de", "dag"]
    , ["de+closed", "tek"]
    , ["tut"]
    , ["ka"]
    , ["pang"] -- left
    , ["pak"]
    , ["de+left", "tut+left"]
    ]


-- * pakhawaj

pakhawajDir = baseDir </> "pakhawaj1/norm"

pakhawajGroups :: Rename.GroupMap
pakhawajGroups = Rename.makePitches 12 6 12
    [ ["ki"] -- left
    , ["ge"]
    , ["tet"] -- right
    , ["te"]
    , ["ne"]
    , ["na", "nam"]
    , ["din"]
    , ["ta"]
    , ["di"]
    ]

-- * gender wayang

wayangDir = baseDir </> "wayang/gede-isep"
renameWayang = renameP wayangGroups wayangDir
wayangGroups = Pitched.permuteDs wayangPemadeIsep

wayangPemadeIsep :: [D]
wayangPemadeIsep =
    -- [ w "mute"          8 1
    [ w "loose"         8 1
    ]
    where w = makeWayang

wayangPemade :: [D]
wayangPemade =
    [ w "open"          4 5
    , w "mute"          8 0
    , w "calung"        3 5
    , w "calung+mute"   6 0
    , w "loose"         8 0
    ]
    where w = makeWayang

wayangKantilan :: [D]
wayangKantilan =
    [ w "open"          4 6
    , w "mute"          8 2
    , w "calung"        3 6
    , w "calung+mute"   6 2
    , w "loose"         8 2
    ]
    where w = makeWayang

makeWayang :: String -> Int -> Key.Key -> D
makeWayang articulation variations octave =
    D (Pitch pitches)
        [D (Art [articulation]) [D (Dyn dyns) [D (Var variations) []]]]
    where
    pitches = map (oct octave) [o, e, u, a]
        ++ map (oct (octave+1)) [i, o, e, u, a]
        ++ map (oct (octave+2)) [i]
    (i, o, e, u, a) = (Key.e_1, Key.f_1, Key.a_1, Key.b_1, Key.c0)
    oct n k = k + n * 12
    dyns = [(1, 31), (32, 64), (65, 108), (109, 127)]
